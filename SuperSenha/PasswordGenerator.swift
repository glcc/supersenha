//
//  PasswordGenerator.swift
//  SuperSenha
//
//  Created by Gerson  on 11/08/2018.
//  Copyright © 2018 Gerson . All rights reserved.
//

import Foundation

class PasswordGenerator {
    
    var numberOfCharaters: Int
    //var numberOfPasswords: Int
    var useLetter: Bool
    var useNumbers: Bool
    var useCapitalLetter: Bool
    var useSpecialCharacters: Bool
    var passwords = [String]()
    
    private let letter = "qwertyuiopasdfghjklzxcvbnm"
    private let numbers = "1234567890"
    private let specialCharacters = "][?/<~#`!@$%^&*()+=}|:\";',>{ "
    
    init(numberOfCharacters: Int, useLetters: Bool, useNumbers: Bool, useCapitalLetters: Bool, useSpecialCharaters: Bool) {
        
        var numchars = min(numberOfCharacters, 16)
        numchars = max(numchars, 1)
        
        self.numberOfCharaters = numchars
        self.useLetter = useLetters
        self.useNumbers = useNumbers
        self.useCapitalLetter = useCapitalLetters
        self.useSpecialCharacters = useSpecialCharaters
    }
    
    func generate(total: Int) -> [String] {
        passwords.removeAll()
        var universe: String = ""
        
        if useLetter {
            universe += letter
        }
        if useNumbers {
            universe += numbers
        }
        if useSpecialCharacters {
            universe += specialCharacters
        }
        if useCapitalLetter {
            universe += letter.uppercased()
        }
        
        let universeArray = Array(universe)
        while passwords.count < total {
            var password = ""
            for _ in 1...numberOfCharaters {
                let index = Int(arc4random_uniform(UInt32(universeArray.count)))
                password += String(universeArray[index])
            }
            passwords.append(password)
        }
        
        return passwords
    }
}
