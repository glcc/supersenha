//
//  ViewController.swift
//  SuperSenha
//
//  Created by Gerson  on 09/08/2018.
//  Copyright © 2018 Gerson . All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tfTotalPassword: UITextField!
    @IBOutlet weak var tfTotalCharacters: UITextField!
    @IBOutlet weak var swLetters: UISwitch!
    @IBOutlet weak var swNumbers: UISwitch!
    @IBOutlet weak var swCapitalLetters: UISwitch!
    @IBOutlet weak var swSpecialCharaters: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let passwordVC = segue.destination as! PasswordViewController
        if let numberOfPasswords = Int(tfTotalPassword.text!) {
            passwordVC.numberOfPassword = numberOfPasswords
        }
        if let numberOfCharacters = Int(tfTotalCharacters.text!) {
            passwordVC.numberOfCharacters = numberOfCharacters
        }
        passwordVC.useLetters = swLetters.isOn
        passwordVC.useNumbers = swNumbers.isOn
        passwordVC.useCapitalLetters = swCapitalLetters.isOn
        passwordVC.useSpecialCharaters = swSpecialCharaters.isOn
        
        view.endEditing(true)
        
    }


}

